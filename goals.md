# Goals
- C like
- Proper module system
- Practical
- Easy to use
- Minimalistic
- No surprises
- No exceptions
- Explicit is better than implicit
- Easy and fast to parse for both humans and computers


```

type bank_account { 
    let balance = 0;       
    message deposit(int amount) {
        
    }
}

fn foo() {
    print('foo');
}

fn bar() -> int {
    int a = 4, b = 5;
    print(a + b);
    return a + b;    
}

fn main() {
    foo();
}
```

```
bank_account = () {
    /// option 1
    obj = object();
    
    obj.balance = 0;
    
    obj.deposit = (self, amount) {
        self.balance += amount;
    };
    
    obj.withdraw = (self, amount) {
        self.balance -= amount;
    };
    
    return obj;
    
    /// Option 2
    obj = object(
        attributes = [balance],
        new = (self) {
            self.balance = 0
        },
        messages = [
            deposit = (self, amount) {
                self.balance += amount;
            },            
            withdraw = (self, amount) {
                self.balance -= amount;
            }        
        ],
    );       
    
    /// Option 3
    obj = object(
        balance = 0,
        deposit = (self, amount) {
            self.balance += amount;
        },
        withdraw = (self, amount) {
            self.balance -= amount;
        }
    );
    
    return obj;
};

ba1 = bank_account();
ba1.deposit(100)
```